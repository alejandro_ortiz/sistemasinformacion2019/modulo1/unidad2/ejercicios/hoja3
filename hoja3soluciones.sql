﻿USE ciclistas;

-- 1. Listar las edades de todos los ciclistas de Banesto

  SELECT 
    DISTINCT c.edad 
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo='Banesto';

-- 2. Listar las edades de los ciclistas que son de Banesto o de Navigare

  SELECT 
    DISTINCT c.edad 
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo='Banesto'
  UNION
  SELECT 
    DISTINCT c.edad 
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo='Navigare';

-- 3. Listar el dorsal de los ciclistas que son de Banesto y cuya edad esta entre 25 y 32

  SELECT 
    c.dorsal 
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo='Banesto' 
    AND 
    c.edad BETWEEN 25 AND 32;

-- 4. Listar el dorsal de los ciclistas que son de Banesto o cuya edad esta entre 25 y 32

  SELECT 
    c.dorsal 
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo='Banesto'
  UNION
  SELECT 
    c.dorsal 
  FROM 
    ciclista c 
  WHERE
    c.edad BETWEEN 25 AND 32;

-- 5. Listar la inicial del equipo de los ciclistas cuyo nombre comience por R

  SELECT 
    DISTINCT LEFT(c.nomequipo,1) 
  FROM 
    ciclista c 
  WHERE 
    c.nombre LIKE 'R%';

-- 6. Listar el código de las etapas que su salida y llegada sea en la misma población

  SELECT 
    e.numetapa 
  FROM 
    etapa e 
  WHERE 
    e.salida=e.llegada;

-- 7. Listar el código de las etapas que su salida y llegada no sea en la misma población y que conozcamos el dorsal del ciclista que ha ganado la etapa

  SELECT 
    e.numetapa 
  FROM 
    etapa e 
  WHERE 
    e.salida<>e.llegada 
    AND 
    e.dorsal IS NOT NULL;

-- 8. Listar le nombre de los puertos cuya altura entre 1000 y 2000 o que la altura sea mayor que 2400

  SELECT 
    p.nompuerto 
  FROM 
    puerto p 
  WHERE 
    p.altura BETWEEN 1000 AND 2000
  UNION
  SELECT 
    p.nompuerto 
  FROM 
    puerto p 
  WHERE 
    p.altura>2400;

-- 9. Listar el dorsal de los ciclistas que hayan ganado los puertos cuya altura entre 1000 y 2000 o que la altura sea mayor de 2400

  SELECT 
    DISTINCT p.dorsal
  FROM 
    puerto p 
  WHERE 
    p.altura BETWEEN 1000 AND 2000
  UNION
  SELECT 
    DISTINCT p.dorsal 
  FROM 
    puerto p 
  WHERE 
    p.altura>2400;

-- 10. Listar el número de ciclistas que hayan ganado alguna etapa

  SELECT 
    COUNT(DISTINCT e.dorsal)num_ciclistas_victorias 
  FROM 
    etapa e;

-- 11. Listar el número de etapas que tengan puerto

  SELECT 
    COUNT(DISTINCT p.numetapa)etapas_con_puerto 
  FROM 
    puerto p;

-- 12. Listar el número de ciclistas que hayan ganado algún puerto

  SELECT 
    COUNT(DISTINCT p.dorsal)ciclistas_coronan_puerto 
  FROM 
    puerto p;

-- 13. Listar el código de la etapa con el número de puertos que tiene

  SELECT 
    p.numetapa, 
    COUNT(*)num_puertos 
  FROM 
    puerto p 
  GROUP BY 
    p.numetapa;

-- 14. Indicar la altura media de los puertos

  SELECT 
    AVG(p.altura)altura_media 
  FROM 
    puerto p;

-- 15. Indicar el código de etapa cuya altura media de sus puertos está por encima de 1500

  SELECT 
     DISTINCT p.numetapa 
  FROM 
    puerto p 
  GROUP BY 
    p.numetapa 
  HAVING 
    AVG(p.altura)>1500;

-- 16. Indicar el número de etapas que cumplen la condición anterior

  SELECT 
    COUNT(*)etapas_media_mas_1500 
  FROM (
    SELECT 
      DISTINCT p.numetapa 
    FROM 
      puerto p 
    GROUP BY 
      p.numetapa 
    HAVING 
      AVG(p.altura)>1500
    ) c1;

-- 17. Listar el dorsal del ciclista con el número de veces que ha llevado algún maillot

  SELECT 
    l.dorsal, 
    COUNT(*)veces_maillot 
  FROM 
    lleva l 
  GROUP BY 
    l.dorsal;

-- 18. Listar el dorsal del ciclista con el código de maillot y cuantas veces ese ciclista ha llevado ese maillot

  SELECT 
    l.dorsal, 
    l.código, 
    COUNT(*)veces_maillot 
  FROM 
    lleva l 
  GROUP BY 
    l.dorsal,
    l.código;

-- 19. Listar el dorsal, el código de etapa, el ciclista y el número de maillots que ese ciclista a llevado en cada etapa

  SELECT 
    l.dorsal, 
    l.numetapa, 
    COUNT(*)maillots_etapa 
  FROM 
    lleva l 
  GROUP BY 
    l.dorsal, 
    l.numetapa;

  -- mostrando tambien el nombre del ciclista
  SELECT 
    c1.dorsal,
    c1.numetapa,
    c1.maillots_etapa,
    c.nombre 
  FROM (
    SELECT 
      l.dorsal, 
      l.numetapa, 
      COUNT(*)maillots_etapa 
    FROM 
      lleva l 
    GROUP BY 
      l.dorsal, 
      l.numetapa) c1 
  JOIN 
    ciclista c 
  USING 
    (dorsal);